package Model.db;

import javax.persistence.*;

@Entity
@Table(name = "users_data", schema = "coldstepbot")
public class UsersDataEntity {
    private int id;
    private String chatId;
    private String todolistAddress;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "chat_id")
    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    @Basic
    @Column(name = "todolist_address")
    public String getTodolistAddress() {
        return todolistAddress;
    }

    public void setTodolistAddress(String todolistAddress) {
        this.todolistAddress = todolistAddress;
    }


}
