package Controller;

import Model.db.UsersDataEntity;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class WorkWithDB {
    private SessionFactory sessionFactory;

    public WorkWithDB() {
        sessionFactory=new HibernateUtil().getSessionFactory();
    }
    public void endSesionFactory(){
        sessionFactory.close();
    }

    public boolean createNewUser(String chatId, String todoListAddress) {

        UsersDataEntity usersDataEntity = new UsersDataEntity();
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        usersDataEntity.setChatId(chatId);
        usersDataEntity.setTodolistAddress(todoListAddress);

        session.save(usersDataEntity);
        session.getTransaction().commit();
        session.close();

        writeNewTodo(chatId,"");

        return true;
    }
    public String getTodoList(String chatId) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        List list = session.createQuery("select todolistAddress from UsersDataEntity where chatId='" +
                chatId+"'").list();
        String todo ="Your TO-DO list\n" ;
        session.close();
        String temp=null;
        try {
            temp = FileUtils.readFileToString(new File(String.valueOf(list.get(0))),"utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (temp.equals("")){
            return todo="Your To-Do list is empty";
        }else {
            return todo+temp;
        }

    }
    public String writeNewTodo(String chatId,String newTodo)  {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        List list = session.createQuery("select todolistAddress from UsersDataEntity where chatId='" +
                chatId+"'").list();
        session.close();
        File file = new File((String) list.get(0));
        try {
            FileUtils.writeStringToFile(file,newTodo+"\n",true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        session.close();
        return "Your new todo's was saved";
    }
    public String clearTodoList(String chatId)  {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        List list = session.createQuery("select todolistAddress from UsersDataEntity where chatId='" +
                chatId+"'").list();
        session.close();
        File file = new File((String) list.get(0));
        try {
            FileUtils.writeStringToFile(file,"",false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "You todo list clean";
    }
    public boolean isExist(String chatId){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        List list = session.createQuery("select todolistAddress from UsersDataEntity where chatId='"+
                chatId+"'").list();
        session.close();
        if (list.isEmpty()==true){
            return false;
        }else {
            return true;
        }
    }

}
