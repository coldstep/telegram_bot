package Controller;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class FileAnalyzator {
private Properties prop;
private String fileconf = "src/main/resources/config/file.properties";
    public String[] musicAnalyzator(){
        prop = new Properties();
        try {
            prop.load(new FileReader(fileconf));
        } catch (IOException e) {
            e.printStackTrace();
        }
        File f = new File(prop.getProperty("music.path"));
        return f.list();
    }
    public String[] wallpaperAnalyzator(){
        prop = new Properties();
        try {
            prop.load(new FileReader(fileconf));
        } catch (IOException e) {
            e.printStackTrace();
        }
        File f = new File(prop.getProperty("wallpaper.path"));
        return f.list();

    }

}
