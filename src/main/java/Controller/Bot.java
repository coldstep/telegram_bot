package Controller;

import Model.weather.WeatherResult;
import com.google.gson.Gson;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import org.telegram.telegrambots.api.methods.send.SendAudio;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.Audio;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

public class Bot extends TelegramLongPollingBot {
    private static boolean saveStatus=false;
    private static boolean buttonStatus = false;
    private String botconf ="src/main/resources/config/bot.properties";
    private String fileconf = "src/main/resources/config/file.properties";
    private String contactconf = "src/main/resources/config/contact.properties";
    private String botUserName;
    private String botToken;
    private String musicPath;
    private String wallpaperPath;
    private String todoListPath;
    private WorkWithDB wwdb;
    private List<String> contactsList;

    public Bot() {
        wwdb = new WorkWithDB();
        contactsList = new ArrayList<String>();
        try{
            Properties properties = new Properties();
            properties.load(new FileInputStream(botconf));
            botUserName = properties.getProperty("bot.name");
            botToken = properties.getProperty("bot.token");
            properties.load(new FileReader(fileconf));
            musicPath = properties.getProperty("music.path");
            wallpaperPath = properties.getProperty("wallpaper.path");
            todoListPath = properties.getProperty("todolist.path");
            properties.load(new FileInputStream(contactconf));
            contactsList.add(properties.getProperty("instagram"));
            contactsList.add(properties.getProperty("facebook"));
            contactsList.add(properties.getProperty("github"));
            contactsList.add(properties.getProperty("telegram"));

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    public synchronized void onUpdateReceived(Update update) {
        if (update.hasCallbackQuery()){


            if (update.getCallbackQuery().getData().equals("update1_msg_text")) {
                String answer = "Updated message text";
                EditMessageText new_message = new EditMessageText()
                        .setChatId(update.getCallbackQuery().getMessage().getChatId())
                        .setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                        .setText(answer);
                try {
                    execute(new_message);
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
        }}
        else {
            String message = update.getMessage().getText();
//        System.out.println(message);
            if (saveStatus==false ){
                sendMsg(update.getMessage().getChatId().toString(), message);
            }else {
                update.getMessage().hasInvoice();
                saveNewTodo(update.getMessage().getChatId().toString(), message);
            }
        }



    }


    public synchronized void sendMsg(String chatId, String s)  {
        SendMessage sendMessage;

        if (s.equals("/newwallpaper")) {
            FileAnalyzator fa = new FileAnalyzator();
            List list = new LinkedList<String>();
            String [] analys = fa.wallpaperAnalyzator();
            for (Object object: analys){
                list.add(object);
            }
            SendPhoto photo = new SendPhoto()
                    .setChatId(chatId)
                    .setNewPhoto(new File(wallpaperPath
                            +String.valueOf(((LinkedList) list).getLast())));
            try {
                sendPhoto(photo);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }else if (s.equals("/help")) {
            try {
                    sendMessage = new SendMessage();
                    sendMessage.setChatId(chatId);
                    sendMessage.setText("Help");
                    new SetBotButtons().setInline(sendMessage);
                    execute(sendMessage);

            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }else if (s.equals("/converttodotoimage")) {
            try {
                SendPhoto sendPhoto = new SendPhoto()
                        .setChatId(chatId)
                        .setNewPhoto(new File(new ConvertTextToImage().convert(chatId)));
                sendPhoto(sendPhoto);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }else if(s.equals("/musicplaylist")){
            FileAnalyzator fa = new FileAnalyzator();
            List list = new ArrayList<String>();
            String [] analys = fa.musicAnalyzator();
            for (Object object: analys){
                list.add(object);
            }
            try {
                for(int i = 0; i < 10; i++){
                    SendAudio audio = new SendAudio()
                            .setChatId(chatId)
                            .setNewAudio(new File(musicPath
                                    + list.get((new Random().nextInt(list.size())))));
                    sendAudio(audio);
                }
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

        }else if (s.equals("/showcontacts")) {
            try {
                for (String str: contactsList){
                    sendMessage = new SendMessage();
                    sendMessage.setChatId(chatId);
                    sendMessage.setText(str);
                    execute(sendMessage);
                }
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        } else {
            if (s.equals("/start")) {
                wwdb.createNewUser(chatId,todoListPath+chatId+".txt");
                s = "Hello my friend!\n" +
                        "todolist - show your to-do list\n" +
                        "writenewtodolist - write new to-do's\n" +
                        "cleartodolist - clean your to-do list\n" +
                        "converttodotoimage - convert your to-do list in image and send to you\n" +
                        "musicplaylist - send you random music playlist\n" +
                        "newwallpaper - give you new wallpaper\n" +
                        "shownewvideos - show you new video from your youtube account\n" +
                        "time - show you time in Lviv\n" +
                        "weather - show weather for today\n" +
                        "showcontacts - show you developer contacts\n" +
                        "help - help you";
            }else if (s.equals("/botgoessleep")) {
                System.exit(0);
            } else if (s.equals("How are you ?")) {
                s = "I'm fine )";
            }else if (s.equals("/todolist")) {
                WorkWithDB wwdb = new WorkWithDB();
                s=wwdb.getTodoList(chatId);
            }else if (s.equals("/writenewtodolist")) {
                saveStatus=true;
                s="Write your todo's ";

            }else if (s.equals("/cleartodolist")) {
                WorkWithDB wwdb = new WorkWithDB();
                s=wwdb.clearTodoList(chatId);

            }else if (s.equals("/time")) {
                SimpleDateFormat date_format = new SimpleDateFormat("HH:mm:ss");
                s = date_format.format(new Date());
            }else if (s.equals("/shownewvideos")) {
                s="https://www.youtube.com/feed/subscriptions";
            }else if (s.equals("/weather")) {
                String weatherJson=null;
                try {
                    URL oracle = new URL("https://api.openweathermap.org/data/2.5/weather?q=lviv&APPID=4bb9969e1d07e6dd69a8824e9f15f358&units=metric");

                    BufferedReader in = new BufferedReader(
                                    new InputStreamReader(oracle.openStream()));
                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                            weatherJson = inputLine;
                        }
                    in.close();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }catch (IOException e){
                    e.printStackTrace();
                }
                Gson gson = new Gson();
                WeatherResult weatherResult = gson.fromJson(weatherJson, WeatherResult.class);
                s=(weatherJson==null ? "https://www.gismeteo.ua/ua/"  : weatherResult.toString());
            } else{
                s = "Unknown command";
            }

            sendMessage = new SendMessage();
            sendMessage.enableMarkdown(true);
            sendMessage.setChatId(chatId);
            sendMessage.setText(s);

            try {
                execute(sendMessage);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }
    private void saveNewTodo(String chatId, String info){
        if (info.equals("yes")){

            try {
                SendMessage sendMessage = new SendMessage();
                sendMessage.setChatId(chatId);
                sendMessage.setText("Write todo's");
                execute(sendMessage);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

        }else if (info.equals("no")){
            saveStatus=false;
        }else {
            try {
                execute(new SendMessage()
                        .setChatId(chatId)
                        .setText(wwdb.writeNewTodo(chatId, info)
                                +"Do you want write more todo's?\n" +
                                "yes/no"));
            }catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }

    }

    public String getBotUsername() {
        return botUserName;
    }

    public String getBotToken() {
        return botToken;
    }


}
